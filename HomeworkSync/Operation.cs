﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeworkSync
{
    public class Operation
    {
        private object lockObject = new object();
        private int card = 0;
        
        public void Plus(object money)
        {
            lock (lockObject)
            {
                Console.WriteLine($"***Поток начал работу ({Thread.CurrentThread.ManagedThreadId})");
                //var account = new Account();
                Console.WriteLine($"+ Счет на текущий момент: {card}");
                card += (int)money;
                Console.WriteLine($"+ Счет после операции: {card}");
                Thread.Sleep(5 * new Random().Next(100));
            }
        }

        public void Minus(object money)
        {
            lock (lockObject)
            {
                Console.WriteLine($"***Поток начал работу ({Thread.CurrentThread.ManagedThreadId})");
                //var account = new Account();
                Console.WriteLine($"- Счет на текущий момент: {card}");
                card -= (int)money;
                Console.WriteLine($"- Счет после операции: {card}");
                Thread.Sleep(5 * new Random().Next(100));
            }
        }


        public void NotSyncPlus(object money)
        {
            Thread.Sleep(100);
            Console.WriteLine($"***Поток начал работу ({Thread.CurrentThread.ManagedThreadId})");
            //var account = new Account();
            Console.WriteLine($"+ Счет на текущий момент: {card}");
            card += (int)money;
            Console.WriteLine($"+ Счет после операции: {card}");
        }

        public void NotSyncMinus(object money)
        {
            Thread.Sleep(350);
            Console.WriteLine($"***Поток начал работу ({Thread.CurrentThread.ManagedThreadId})");
            //var account = new Account();
            Console.WriteLine($"- Счет на текущий момент: {card}");
            card -= (int)money;
            Console.WriteLine($"- Счет после операции: {card}");
        }
    }
}
