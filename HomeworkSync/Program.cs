﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HomeworkSync
{
    class Program
    {
        static void Main(string[] args)
        { 
            var operation = new Operation();

            

            var threads = new Thread[60];
            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(operation.NotSyncPlus); 
            }

            foreach (var thread in threads)
            {
                thread.Start(200);
            }

            for (int i = 0; i < threads.Length; i++)
            {
                threads[i] = new Thread(operation.NotSyncMinus);
            }

            foreach (var thread in threads)
            {
                thread.Start(100);
            }
            Console.ReadLine();

        }
    }
}
